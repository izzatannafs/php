<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");
echo "================ Ini adalah Shaun ================ <br>";

echo $sheep->name;
echo "<br>";
echo $sheep->legs;
echo "<br>";
echo $sheep->cold_blooded;
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "============== Ini adalah Kera Sakti ================ <br>";
$sungokong->yell();

echo "<br><br>";

$kodok = new Frog("buduk");
echo "================ Ini adalah Kodok ================ <br>";
$kodok->jump();




?>